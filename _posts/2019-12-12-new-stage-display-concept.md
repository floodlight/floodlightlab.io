---
title: New Stage Display Concept
layout: post
---

I've been thinking for a while about how to make the Stage Display more useful.
The current stage display looks like this:

![Screenshot of the old stage display](/assets/posts/2019-12-12-old-stage-display.png)

I like a lot of things about this design. It's nice and simple. The lyrics flow
easily from the current slide to the next, rather than the next slide being in a
separate box. And it automatically switches between showing lyrics and media
based on what's on the slide, rather than setting a layout cue.

However, there's one major problem: the lyrics are way too narrow. This often
causes them to take up more lines than necessary, making it harder to read.
Also, there's sometimes not much space left for the lyrics of the next slide.

The reason it's so narrow is to fit the clock and the segment notes (which are
empty in this screenshot).

## The New Design

![Screenshot of the new stage display](/assets/posts/2019-12-12-new-stage-display.png)

As you can see, the width problem has been fixed. The clock is now in the top
left, and the lyrics take up the whole width of the screen if there aren't any
segment notes. (If there are notes, it will behave like before--the lyrics will
become narrower to make space).

Another addition is the colors along the side. I call this the "locator
sidebar". It indicates which slide you're on within a segment. The colors are
the same as in the Slides view, so you can see, for example, which chorus you're
on, at a glance.

## Future Plans

I'd like to add some new settings for the Stage Display. In particular, there
should be options for:

- Font size
- Hiding the locator sidebar

Look out for these improvements and others in the next release of Floodlight.

Suggestions are welcome! See "How can I contact you?" on the [FAQ](/faq).

## Final Thoughts
The ultimate goal of a stage display, and of church presentation software in
general, is to stay out of the way and help worship take place without
distractions. Hopefully, this new stage display design will support that goal.
