---
title: Floodlight 0.9.0 released
layout: post
tags: changelog
---

Floodlight 0.9.0 has been released, marking the start of the public changelog!

New Features:
- Import songs with a simple copy/paste
  <https://floodlight.gitlab.io/presenter/usermanual/getting-started/your-first-playlist#from-lyrics-files>

Changes:
- Slide text now wraps instead of shrinking. This prevents long lines from
  being too small to read.
