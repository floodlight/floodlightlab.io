---
title: Floodlight 0.10.0 released
layout: post
tags: changelog
---

With the release of 0.10.0, Floodlight is officially in *public beta!* I'm
looking for people to help me test it. If that sounds like fun, head to
[the downloads page](/download) and try it out!

New Features:
- Update notifications: There is now an in-app update prompt so you can make
  sure Floodlight stays up to date with the latest features and fixes
- When importing multiple images, choose to import them as a slideshow or as
  individual images
- Drag-and-drop to organize your playlists in the Playlist Manager
- Delete stuff from your Library
- Pause and seek videos
- Set a duration for images. This allows you to make announcement loops with
  static images as well as videos.

Changes:
- Changed the colors used for verses, choruses, bridges, etc.

Removed:
- Arrangements have been removed.
