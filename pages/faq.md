---
layout: page
title: Frequently Asked Questions
permalink: /faq
---

## Why is it free?
Floodlight is a ministry, not a company.

Most similar software is either several hundred dollars or costs a monthly fee.
I created Floodlight because I didn't like that. Why should churches pay so
much for software, when projects like VLC, HandBrake, and Firefox prove that
good software can be free?

## Why doesn't it do X or Y?
Floodlight is a purely volunteer-based effort. Time and resources are limited,
so not every feature on the wishlist is feasible. If you'd like to help, see
the [Getting Involved](/get-involved) guide.

Also, some features just aren't in the scope of what Floodlight is designed to
do. It's a simple, user-friendly worship presentation program. It's not a
professional software suite, and it's not intended for concerts. Feature
requests that don't further the goals of the project will be declined.

## How can I contact you?
For bug reports or feature requests, create an account on
[GitLab](https://gitlab.com) and post a new issue
[here](https://gitlab.com/floodlight/presenter/issues/new).

For general discussion, support, etc.
[join us on Discord](https://discord.gg/j3hQCr2). You can also join the
conversation via
[your favorite Matrix app](https://matrix.to/#/#floodlight:matrix.org).

## Floodlight doesn't work on my Mac.
Make sure your Mac is up to date, running the latest version of macOS.
Floodlight is only tested on the latest version; unfortunately, it takes too
much time to maintain support for old versions. Besides, keeping your computer
up to date is important to keep it secure.
