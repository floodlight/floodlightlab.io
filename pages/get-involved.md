---
layout: page
title: Get Involved
permalink: /get-involved
---

Floodlight is a volunteer effort, and there are many ways to volunteer!

## Spread the Word
If you like Floodlight, tell people about it! That's an easy way to help out
that anyone can do.

## Tutorials/User Manual
The [user manual](/presenter/usermanual) can always use improvement. This includes
documenting obscure features or writing how-to articles or tutorials.

## Coding
If you know how to code, your help would be greatly appreciated! Start on the
[issue tracker](https://gitlab.com/floodlight/presenter/issues) and see what
looks easy or interesting to fix, or look for issues labeled "Newcomers".

Presenter is written in [Vala](https://wiki.gnome.org/Projects/Vala), a
compiled language with similarities to C#. It is quite easy to learn.
