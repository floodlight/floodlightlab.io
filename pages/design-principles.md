---
layout: page
title: Design Principles
permalink: /design-principles
---

These are the basic principles driving the design of Floodlight. They guide all
decisions about how it works and what features are added.

## User Friendliness
Floodlight intends to be as user friendly as possible. It shouldn't take a
tech-savvy volunteer to install or use it. Tutorials should be clear and
concise, and the user interface should be intuitive enough that new users catch
on quickly.

## Predictability
The software should avoid doing things unexpectedly, which might lead to
mistakes. In every situation, it should do everything it can to avoid glitches
or problems.

## Flexibility
Different ministries have different needs, and to be useful, Floodlight needs
to accommodate those needs. All new features should be considered in the
context of the rest of the program, rather than tacking on new buttons and
switches as they are asked for.

When a feature is requested, it's important to know why. Think in terms of the
problem, not the solution: is this feature really needed, or is there a better
way? This helps avoid a number of problems down the road.
